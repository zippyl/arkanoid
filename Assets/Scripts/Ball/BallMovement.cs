﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMovement : MonoBehaviour
{

    public enum Direction { Left, Right }

    [SerializeField] private float movementSpeed = 10f;
    [SerializeField] private float screenPosOffset = 0.1f;
    [SerializeField] private float minDistance = 0.1f;

    [Header("Block side")]
    [SerializeField] private Transform corner;
    [SerializeField] private Transform side;

    [SerializeField] private GameObject targetBlock;
    private bool canReflect = true;
    private float tempMinDistance;
    private float angleReflection = 90f;
    private GameController gameController;
    private Direction direction;

    // Use this for initialization
    void Start()
    {
        gameController = FindObjectOfType<GameController>();
        tempMinDistance = minDistance;
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        DetectPlayer();
        DetectCornerOrSide();

    }

    private void Move()
    {
        transform.Translate(Vector2.up * Time.deltaTime * movementSpeed);
        Vector3 playerOnScreenPos = Camera.main.WorldToScreenPoint(transform.position);
        if (playerOnScreenPos.x <= screenPosOffset)
        { direction = Direction.Right; ReflectAngle(); }
        if (playerOnScreenPos.x >= (Screen.width - screenPosOffset))
        { direction = Direction.Left; ReflectAngle(); }

        if (playerOnScreenPos.y <= screenPosOffset || playerOnScreenPos.y >= (Screen.height - screenPosOffset))
        {
            ReflectAngle();
        }
    }

    private void DetectCornerOrSide()
    {
        targetBlock = GetClosestBlock(gameController.Blocks.ToArray());
        if (targetBlock != null && Vector3.Distance(transform.position, targetBlock.transform.position) < 0.4f)
        {
            corner = GetClosetPoint(targetBlock.GetComponent<BlockController>().GetCorners());
            if (corner != null && Vector2.Distance(transform.position, corner.position) <= 0.2f)
            {
                if (direction == Direction.Left)
                    ReflectAngle(angleReflection * 2f);
                if (direction == Direction.Right)
                    ReflectAngle(-angleReflection * 2f);
                ByeeeBlock();
                return;
            }
            if (direction == Direction.Left)
                ReflectAngle(angleReflection);
            if (direction == Direction.Right)
                ReflectAngle(-angleReflection);
            ByeeeBlock();
        }
        else
        {
            canReflect = true;
        }
    }

    private void ByeeeBlock()
    {
        foreach (var item in gameController.Blocks)
        {
            if (item.name == targetBlock.name)
            {
                item.SetActive(false);
                gameController.Blocks.Remove(item);
                gameController.UpdateBlockArray();
                break;
            }
        }
    }

    private void DetectPlayer()
    {
        var corner = GetClosetPoint(gameController.Player.GetComponent<PlayerMovement>().GetCorners());
        var side = GetClosetPoint(gameController.Player.GetComponent<PlayerMovement>().GetSides());
        if (corner != null && Vector2.Distance(transform.position, corner.position) <= 0.2f)
        {
            ReflectAngle(angleReflection * 2f);
        }
        if (side != null && Vector2.Distance(transform.position, side.position) <= 0.2f)
        {
            ReflectAngle();
        }
    }

    private GameObject GetClosestBlock(GameObject[] gameObjects)
    {
        GameObject targetBlock = null;
        Vector3 currentPosition = transform.position;
        foreach (var item in gameObjects)
        {
            float distance = Vector3.Distance(item.transform.position, currentPosition);
            if (distance < minDistance)
            {
                targetBlock = item;
                minDistance = distance;
            }
        }
        return targetBlock;
    }

    private Transform GetClosetPoint(Transform[] transforms)
    {
        Transform targetPoint = null;
        Vector3 currentPosition = transform.position;
        foreach (var item in transforms)
        {
            float distance = Vector3.Distance(item.transform.position, currentPosition);
            if (distance < minDistance)
            {
                targetPoint = item;
                minDistance = distance;
            }
        }
        return targetPoint;
    }

    private void ReflectAngle()
    {
        if (canReflect)
        {
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z - angleReflection);
            minDistance = tempMinDistance;
            canReflect = false;
        }
    }

    private void ReflectAngle(float value)
    {
        if (canReflect)
        {
            transform.rotation = Quaternion.Euler(0, 0, transform.rotation.eulerAngles.z + value);
            minDistance = tempMinDistance;
            canReflect = false;
        }
    }
}
