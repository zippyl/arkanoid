﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockController : MonoBehaviour
{

    [System.Serializable]
    public struct Side
    {
        public Transform left;
        public Transform right;
        public Transform top;
        public Transform bottom;
    }

    [System.Serializable]
    public struct Corner
    {
        public Transform leftTop;
        public Transform rightTop;
        public Transform rightBottom;
        public Transform leftBottom;
    }

    public Side side;
    public Corner corner;

    public Transform[] GetSides()
    {
        Transform[] sidesCollection = { side.left, side.right, side.top, side.bottom };
        return sidesCollection;
    }

    public Transform[] GetCorners()
    {
        Transform[] cornerCollection = { corner.leftTop, corner.rightTop, corner.rightBottom, corner.leftBottom };
        return cornerCollection;
    }
}
