﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{

    [SerializeField] private List<GameObject> blocks;
    [SerializeField] private GameObject player;

    private UIController uiController;

    // Use this for initialization
    void Start()
    {
        var blockCollection = GameObject.FindGameObjectsWithTag("Block");
        blocks.AddRange(blockCollection);
        player = GameObject.FindGameObjectWithTag("Player");
        uiController = FindObjectOfType<UIController>();
    }

    public void UpdateBlockArray()
    {
        if(blocks.Count <= 0)
        {
            uiController.GameOver();
        }
    }

    public List<GameObject> Blocks { get { return blocks; } }
    public GameObject Player { get { return player; } }
}
