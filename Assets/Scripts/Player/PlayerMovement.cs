﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [System.Serializable]
    public struct Corner
    {
        public Transform leftTop;
        public Transform rightTop;
        public Transform rightBottom;
        public Transform leftBottom;

        public Transform left;
        public Transform right;
        public Transform top;
        public Transform bottom;
        public Transform centerTop;
        public Transform centerBottom;
    }

    public Corner corner;

    private GameController gameController;

    // Use this for initialization
    void Start()
    {
        gameController = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = new Vector2(mousePosition.x, transform.position.y);
        }
    }

    public Transform[] GetCorners()
    {
        Transform[] cornerCollection = { corner.leftTop, corner.rightTop, corner.rightBottom, corner.leftBottom };
        return cornerCollection;
    }

    public Transform[] GetSides()
    {
        Transform[] sidesCollection = { corner.left, corner.right, corner.top, corner.bottom, corner.centerTop, corner.centerBottom };
        return sidesCollection;
    }
}
