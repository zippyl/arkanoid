﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour {

    [SerializeField] private GameObject gameOverPanel;

    private void Start()
    {
        SetTimeScale(1f);
    }

    public void GameOver()
    {
        SetTimeScale(0.00000001f);
        gameOverPanel.SetActive(true);
    }

    public void Exit()
    {
        Application.Quit();
    }

    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void SetTimeScale(float value)
    {
        Time.timeScale = value;
    }
}
